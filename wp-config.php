<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wplearning_05' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '},o08 ct_UVKcDLx0<b`}AF@rUV$)+KbdVj4m:&YY94wI&Foe4IobJKPB9;7lG*d' );
define( 'SECURE_AUTH_KEY',  'S9Or0#4]:=pBEtdsf.Lf@Dc$&yNB7,+:{LN5{;T|!i<C#pQ,obgR5`DR%+Ieykk)' );
define( 'LOGGED_IN_KEY',    'hPBkSidNMVk|^}.1c!gX=#-oAe<^qG-c`2K*$Z,I7KmWCOl1Dm<]Q;!sTo0nTJm+' );
define( 'NONCE_KEY',        '?~x=B,9AFTcw(#1;JM@[?rxH!Ux=_Y J(q$/D?-I7(4Qs%|4 )K>!$]8B]mCsyG`' );
define( 'AUTH_SALT',        'n)$#$M/b1DeK}^]2e-8KYoZH#p,Z=`_bH!D1+(/J06JQk~Gb[RV?^k_j#Jm,KEwS' );
define( 'SECURE_AUTH_SALT', 'arNI8CGVoFoszwu~>xOAtm$pV3i/bFLLeM8M3i)adIdM9mAU|G(I|@_B 0^dJM3d' );
define( 'LOGGED_IN_SALT',   '{6EDT&i429_%*<IXNQWtw*5:Q=BB;y_<|)DH>X0AiYKzeIXuK~NGb9`hW}D07QMo' );
define( 'NONCE_SALT',       '+dEoP({cAAy%zIC!R72P{oU~^]|BO :*.Z;u1}v:&w.2-yxj,B^uD+Y/!Z~TGl~y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
