<div id="sidebar">
    <div class="col-md-3">
        <!---- Start Widget ---->
        <div class="widget wid-vid">
            <div class="heading">
                <h4>Videos</h4>
            </div>
            <div class="content">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#most">Most Plays</a></li>
                    <li><a data-toggle="tab" href="#popular">Popular</a></li>
                    <li><a data-toggle="tab" href="#random">Random</a></li>
                </ul>
                <div class="tab-content">
                    <div id="most" class="tab-pane fade in active">
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="youtube">Youtube</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="vimeo">Vimeo</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="youtube">Youtube</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="popular" class="tab-pane fade">
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="youtube">Youtube</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="youtube">Youtube</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="vimeo">Vimeo</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="random" class="tab-pane fade">
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="vimeo">Vimeo</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="vimeo">Vimeo</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                        <div class="post wrap-vid">
                            <div class="zoom-container">
                                <div class="zoom-caption">
                                    <span class="vimeo">Vimeo</span>
                                    <a href="single.html">
                                        <i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
                                    </a>
                                    <p>Video's Name</p>
                                </div>
                                <img src="<?php echo get_template_directory_uri()?>/images/4.jpg" />
                            </div>
                            <div class="wrapper">
                                <h5 class="vid-name"><a href="#">Video's Name</a></h5>
                                <div class="info">
                                    <h6>By <a href="#">Kelvin</a></h6>
                                    <span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i>25/3/2015</span>
                                    <span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---- Start Widget ---->
        <div class="widget wid-gallery">
            <div class="heading"><h4>Gallery</h4></div>
            <div class="content">
                <div class="col-md-4">
                    <div class="row">
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/14.jpg" /></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/15.jpg" /></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/20.jpg" /></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/16.jpg" /></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/17.jpg" /></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/21.jpg" /></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/18.jpg" /></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/19.jpg" /></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri()?>/images/14.jpg" /></a>
                    </div>
                </div>
            </div>
        </div>
        <!---- Start Widget ---->
        <div class="widget wid-new-post">
            <div class="heading"><h4>New Posts</h4></div>
            <div class="content">
                <h6>Lorem Ipsum is simply dummy</h6>
                <img src="<?php echo get_template_directory_uri()?>/images/22.jpg" />
                <ul class="list-inline">
                    <li><i class="fa fa-calendar"></i>25/3/2015</li>
                    <li><i class="fa fa-comments"></i>1,200</li>
                </ul>
                <p>Title should not overflow the content area A few things to check for: Non-breaking text in the...</p>
                <h6>Lorem Ipsum is simply dummy</h6>
                <img src="<?php echo get_template_directory_uri()?>/images/23.jpg" />
                <ul class="list-inline">
                    <li><i class="fa fa-calendar"></i>25/3/2015</li>
                    <li><i class="fa fa-comments"></i>1,200</li>
                </ul>
                <p>Title should not overflow the content area A few things to check for: Non-breaking text in the...</p>
                <h6>Lorem Ipsum is simply dummy</h6>
                <img src="<?php echo get_template_directory_uri()?>/images/24.jpg" />
                <ul class="list-inline">
                    <li><i class="fa fa-calendar"></i>25/3/2015</li>
                    <li><i class="fa fa-comments"></i>1,200</li>
                </ul>
                <p>Title should not overflow the content area A few things to check for: Non-breaking text in the...</p>
            </div>
        </div>
        <!---- Start Widget ---->
        <div class="widget wid-recent-post">
            <div class="heading"><h4>Recent Posts</h4></div>
            <div class="content">
                <span>Creativity is about the journey <a href="#">your mind takes</a></span>
                <span>About the journey <a href="#">your mind</a></span>
                <span>The journey <a href="#">your</a></span>
                <span>Journey is about the journey <a href="#">your mind mind</a></span>
                <span>Creativity is about the journey <a href="#">your mind takes</a></span>
                <span>About the journey <a href="#">your mind</a></span>

            </div>
        </div>
    </div>
    <div class="col-md-3">

        <!---- Start Widget ---->
        <?php dynamic_sidebar('home_right_widget')?>
        <!---- Start Widget ---->
        <div class="widget wid-categoty">
            <div class="heading"><h4>Category</h4></div>
            <div class="content">
                <select class="col-md-12">
                    <option>Mustard</option>
                    <option>Ketchup</option>
                    <option>Relish</option>
                </select>
            </div>
        </div>
        <!---- Start Widget ---->
        <div class="widget wid-calendar">
            <div class="heading"><h4>Calendar</h4></div>
            <div class="content">
                <center><form action="" role="form">
                        <div class="">
                            <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">                </div>
                            <input type="hidden" id="dtp_input2" value="" /><br/>
                        </div>
                    </form></center>
            </div>
        </div>
        <!---- Start Widget ---->
        <div class="widget wid-tweet">
            <div class="heading"><h4>Tweet</h4></div>
            <div class="content">
                <div class="tweet-item">
                    <p><i class="fa fa-twitter"></i> TLAS - Coming Soon PSD Mockup</p>
                    <a>https://t.co/dLsNZYGVbJ</a>
                    <a>#psd</a>
                    <a>#freebie</a>
                    <a>#freebie</a>
                    <a>#dribbble</a>
                    <span>July 15th, 2015</span>
                </div>
                <div class="tweet-item">
                    <p><i class="fa fa-twitter"></i> Little Dude Character With Multiple Hairs</p>
                    <a>https://t.co/dLsNZYGVbJ</a>
                    <a>#freebie</a>
                    <a>#design</a>
                    <a>#illustrator</a>
                    <a>#dribbble</a>
                    <span>June 23rd, 2015</span>
                </div>
                <div class="tweet-item">
                    <p><i class="fa fa-twitter"></i> Newsletter Subscription Form Mockup</p>
                    <a>https://t.co/dLsNZYGVbJ</a>
                    <a>#psd</a>
                    <a>#freebie</a>
                    <a>#freebie</a>
                    <a>#dribbble</a>
                    <span>June 22nd, 2015</span>
                </div>
                <p>Marshall, Will, and Holly on a routine expedition, met the greatest earthquake ever known. High on the rapids, it struck their tiny raft...</p>
            </div>
        </div>

    </div>
</div>
