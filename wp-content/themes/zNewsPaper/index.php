<?php
/*
  * Template Name: Blog Page
  */
?>


<?php get_header()?>
	
	<!-- /////////////////////////////////////////Content -->
	<div id="page-content" class="archive-page container">
		<div class="">
			<div class="row">
				<div id="main-content" class="col-md-8">
                    <?php while(have_posts()): the_post()?>
					<div class="box">
						<a href="<?php the_permalink()?>"><h2 class="vid-name"><?php the_title()?></h2></a>
						<div class="info">
							<h5>By <a href="#"><?php the_author_posts_link()?></a></h5>
							<span><i class="fa fa-calendar"></i> <?php the_date('F j, Y');?></span>
							<span><i class="fa fa-comment"></i> <?php comments_popup_link('No Comments','1 Comment','% Comments','comment_class','Comments off')?></span>
							<span><i class="fa fa-heart"></i>1,200</span>
							<ul class="list-inline">
								<li><a href="#">Rate</a></li>
								<li> - </li>
								<li>
									<span class="rating">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
									</span>
								</li>
							</ul>
						</div>
						<div class="wrap-vid">
							<div class="zoom-container">
								<div class="zoom-caption">
								</div>
								<?php the_post_thumbnail()?>
							</div>
                            <?php the_excerpt()?>
							<a href="<?php the_permalink()?>">MORE...</a>
						</div>
					</div>
					<hr class="line">
                    <?php endwhile;?>
					<div class="box">
                        <center>
                            <?php
                                the_posts_pagination(array(
                                        'mid_size'  => 1,
                                       'prev_text'   => '<span aria-hidden="true">&laquo;</span>',
                                       'next_text'   => '<span aria-hidden="true">&raquo;</span>',
                                       'screen_reader_text'   => ' ',
                                ));
                            ?>
                        </center>
					</div>
				</div>
				<?php get_sidebar()?>
			</div>
		</div>
	</div>

	<?php get_footer()?>
