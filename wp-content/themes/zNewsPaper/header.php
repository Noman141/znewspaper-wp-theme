<!DOCTYPE html>
<html lang="<?php language_attributes()?>">
<head>
    <meta charset="<?php bloginfo('charset')?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free Bootstrap Themes by Zerotheme dot com - Free Responsive Html5 Templates">
    <meta name="author" content="https://www.Zerotheme.com">

    <title><?php bloginfo('name')?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/bootstrap.min.css"  type="text/css">

    <link href="<?php echo get_template_directory_uri()?>/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/style.css">

    <!-- Owl Carousel Assets -->
    <link href="<?php echo get_template_directory_uri()?>/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri()?>/owl-carousel/owl.theme.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/font-awesome-4.4.0/css/font-awesome.min.css"  type="text/css">

    <!-- jQuery and Modernizr-->
    <script src="<?php echo get_template_directory_uri()?>/js/jquery-2.1.1.js"></script>

    <!-- Core JavaScript Files -->
    <script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri()?>/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<header>
    <!--Top-->
    <nav id="top">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php dynamic_sidebar('wc_msg')?>
                </div>
                <div class="col-md-6">
                    <?php
                        wp_nav_menu(array(
                                'theme_location'   => 'top_menu',
                                'menu_class'   => 'list-inline top-link link',
                        ))
                    ?>
                </div>
            </div>
        </div>
    </nav>

    <!--Navigation-->
    <nav id="menu" class="navbar container">
        <div class="navbar-header">
            <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
            <a class="navbar-brand" href="#">
                <div class="logo"><span>Newspaper</span></div>
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <?php wp_nav_menu(array(
                    'theme_location'   => 'main_menu',
                    'menu_class'   => 'nav navbar-nav',
                    'container'   => ' ',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ))?>
            <ul class="list-inline navbar-right top-social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="featured container">
    <div id="owl-demo" class="owl-carousel">
        <?php
            $featured_post = new WP_Query(array(
                    'post_type'   => 'post',
            ));
            while ($featured_post->have_posts()):$featured_post->the_post()
        ?>
        <div class="item">
            <div class="zoom-container">
                <div class="zoom-caption">
<!--                    <span>--><?php //echo the_category()?><!--</span>-->
                    <a href="<?php the_permalink()?>">
                        <i class="fa fa-play-circle-o fa-5x" style="color: #fff"></i>
                    </a>
                    <p><?php the_title()?></p>
                </div>
                <?php the_post_thumbnail()?>
            </div>
        </div>
        <?php endwhile;?>
    </div>
</div>
<!-- Header -->