<?php

add_theme_support('post-thumbnails');

register_sidebar(array(
   'name'     => 'Right Sidebar',
   'id'     => 'right_sidebar',
   'before_title'     => '<div class="heading"><h4>',
   'after_title'     => '</h4></div>',
   'before_widget'     => '<div class="wrap-vid">',
   'after_widget'     => '</div>',
));
register_sidebar(array(
    'name'     => 'Footer Left',
    'id'     => 'footer_left',
    'before_title'     => '<div class="footer-heading"><h1><span style="color: #fff;">',
    'after_title'     => '</span></span></h4></div>',
    'before_widget'     => '<div class="content">',
    'after_widget'     => '</div>',
));
register_sidebar(array(
    'name'     => 'Footer Middle',
    'id'     => 'footer_middle',
    'before_title'     => ' <div class="footer-heading"><h4>',
    'after_title'     => '</h4></div>',
    'before_widget'     => '<div class="content">',
    'after_widget'     => '</div>',
));
register_sidebar(array(
    'name'     => 'Right Right',
    'id'     => 'footer_right',
    'before_title'     => '<div class="footer-heading"><h4>',
    'after_title'     => '</h4></div>',
    'before_widget'     => '<div class="content">',
    'after_widget'     => '</div>',
));
register_sidebar(array(
    'name'     => 'Copy Right',
    'id'     => 'copy_right',
    'before_title'     => ' ',
    'after_title'     => ' ',
    'before_widget'     => '<p>',
    'after_widget'     => '</p>',
));
register_sidebar(array(
    'name'     => 'Welcome Message',
    'id'     => 'wc_msg',
    'before_title'     => ' ',
    'after_title'     => ' ',
    'before_widget'     => '<strong>',
    'after_widget'     => '</strong>',
));
register_sidebar(array(
    'name'     => 'Home Slider Advertisement',
    'id'     => 'home_adv',
    'before_title'     => ' ',
    'after_title'     => ' ',
    'before_widget'     => ' ',
    'after_widget'     => ' ',
));

register_sidebar(array(
    'name'     => 'Home Page Right Widget',
    'id'     => 'home_right_widget',
    'before_title'     => '<div class="heading"><h4>',
    'after_title'     => '</h4></div><div class="content">',
    'before_widget'     => '<div class="widget wid-tags">',
    'after_widget'     => '</div></div>',
));

register_nav_menus(array(
   'top_menu'   => 'Top Menu',
   'main_menu'   => 'Main Menu',
));
require_once get_template_directory() . '/wp-bootstrap-navwalker.php';

function galleryImage(){
    register_post_type('gallery',array(
       'labels'    => array(
           'name'      => 'Gallery',
           'all_items' => 'All Gallery Photos',
           'add_new' => 'Add New Photo',
           'add_new_item' => 'Add New Gallery Photo',
           'featured_image' => 'Gallery Photo',
           'set_featured_image' => 'Set A Gallery Photo',
           'remove_featured_image' => 'Remove The Gallery Photo',
       ),
        'public'    => true,
        'supports'  =>array('title','thumbnail')
    ));
}
add_action('init','galleryImage');