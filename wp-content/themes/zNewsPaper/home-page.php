<?php
/*
  * Template Name: Home Page
  */
?>

<?php get_header()?>
    <div class="featured container">
        <div class="row">
            <div class="col-sm-8">
                <!-- Carousel -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $i = 0;
                            $main_posts_slider = new WP_Query(array(
                               'post_type'   => 'post',
                               'posts_per_page' => 5,
                            ));
                            while ($main_posts_slider->have_posts()):$main_posts_slider->the_post();
                            $class = '';$i++;
                            if ($i==1)$class .= 'active';
                        ?>
                        <div class="item <?php echo $class?>">
                            <?php the_post_thumbnail()?>
                            <!-- Static Header -->
                            <div class="header-text hidden-xs">
                                <div class="col-md-12 text-center">
                                    <h2><?php the_title()?> </h2>
                                </div>
                            </div><!-- /header-text -->
                        </div>

                        <?php
                            endwhile;
                            wp_reset_query()
                        ?>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div><!-- /carousel -->
            </div>
            <div class="col-sm-4" >
                <div >
                    <ul id="owl-demo-1" class="owl-carousel">
                        <?php
                            $carousel_slider = new WP_Query(array(
                               'post_type'    =>'post',
                               'posts_per_page'=> 5,
                            ));
                            if ($carousel_slider->have_posts()):
                        while ($carousel_slider->have_posts()):$carousel_slider->the_post()
                        ?>
                        <li>
                            <a href="<?php the_permalink()?>">
                                <?php the_post_thumbnail()?>
                            </a>
                        </li>
                        <?php endwhile;endif;wp_reset_query();?>
                    </ul>
                </div>
                <?php dynamic_sidebar('home_adv')?>
            </div>
        </div>
    </div>
	
	<!-- /////////////////////////////////////////Content -->
	<div id="page-content" class="index-page container">
		<div class="row">
			<div id="main-content"><!-- background not working -->
				<div class="col-md-6">
                    <?php
                        $leading_post = new WP_Query(array(
                           'posts_type'      =>'post',
                           'posts_per_page'  =>1,
                           'category_name'   =>'leading',
                        ));
                        if ($leading_post->have_posts()):
                            while ($leading_post->have_posts()):$leading_post->the_post()
                    ?>
					<div class="box wrap-vid">
						<div class="zoom-container">
							<div class="zoom-caption">
								<span class="youtube">Youtube</span>
								<a href="single.html">
									<i class="fa fa-play icon-play" style="color: #fff"></i>
								</a>
								<p><?php the_title()?></p>
							</div>
                            <?php the_post_thumbnail()?>
						</div>
						<h3 class="vid-name"><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>
						<div class="info">
							<h5>By <a href="#"><?php the_author()?></a></h5>
							<span><i class="fa fa-calendar"></i><?php the_date('F j, Y');?></span>
							<span><i class="fa fa-heart"></i>1,200</span>
						</div>
						<p class="more"><?php the_excerpt()?></p>
					</div>
                    <?php endwhile;endif;wp_reset_query();?>
					<div class="box">
						<div class="box-header header-vimeo">
							<h2>National</h2>
						</div>
						<div class="box-content">
							<div class="row">
                                <?php
                                $category_leading_post = new WP_Query(array(
                                    'posts_type'      =>'post',
                                    'posts_per_page'  =>1,
                                    'category_name'   =>'national',
                                ));
                                if ($category_leading_post->have_posts()):
                                while ($category_leading_post->have_posts()):$category_leading_post->the_post()
                                ?>
								<div class="col-md-6">
									<div class="wrap-vid">
										<div class="zoom-container">
											<div class="zoom-caption">
												<span class="vimeo">National</span>
												<a href="single.html">
													<i class="fa fa-play-circle-o fa-5x" style="color: #fff"></i>
												</a>
												<p><?php the_title()?></p>
											</div>
											<?php the_post_thumbnail()?>
										</div>
										<h3 class="vid-name"><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>
										<div class="info">
											<h5>By <a href="#"><?php the_author()?></a></h5>
											<span><i class="fa fa-calendar"></i><?php the_date('F j, Y');?></span>
											<span><i class="fa fa-heart"></i>1,200</span>
										</div>
									</div>
									<p class="more"><?php the_excerpt()?></p>
								</div>
                                <?php endwhile;endif;wp_reset_query();?>
								<div class="col-md-6">
                                    <?php
                                    $category_leading_post = new WP_Query(array(
                                        'posts_type'      =>'post',
                                        'posts_per_page'  =>4,
                                        'category_name'   =>'national',
                                    ));
                                    if ($category_leading_post->have_posts()):
                                    while ($category_leading_post->have_posts()):$category_leading_post->the_post()
                                    ?>
									<div class="post wrap-vid row">
										<div class="zoom-container col-md-5">
											<div class="zoom-caption">
												<span class="vimeo">Vimeo</span>
												<a href="<?php the_permalink()?>">
													<i class="fa fa-play-circle-o fa-3x" style="color: #fff"></i>
												</a>
												<p><?php the_title()?></p>
											</div>
											<?php the_post_thumbnail()?>
										</div>
										<div class="wrapper col-md-7">
											<h5 class="vid-name"><a href="<?php the_permalink()?>"><?php the_title()?></a></h5>
											<div class="info">
												<h6>By <a href="#"><?php the_author()?></a></h6>
												<span><i class="fa fa-heart"></i>1,200 / <i class="fa fa-calendar"></i><?php the_date('F j, Y');?></span>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half"></i>
												</span>
											</div>
										</div>
									</div>
                                    <?php endwhile;endif;wp_reset_query();?>
								</div>
							</div>
						</div>
					</div>
					<div class="box">
						<div class="box-header header-photo" style="background: url(<?php echo get_template_directory_uri()?>/images/bg2.jpg);height: 5px">
							<h2>Photos</h2>
						</div>
						<div class="box-content">
							<div id="owl-demo-2" class="owl-carousel">
								<?php
                                    $gallery_image = new WP_Query(array(
                                        'post_type'      =>'gallery',
                                        'post_per_page'  =>9,
                                    ));
                                    if ($gallery_image->have_posts()):
                                    while ($gallery_image->have_posts()):$gallery_image->the_post()
                                ?>
								<div class="item">
									<?php the_post_thumbnail()?>
									<?php the_post_thumbnail()?>
								</div>
                                <?php endwhile;endif;wp_reset_query()?>
							</div>
						</div>
					</div>
					<div class="box">
						<div class="box-header header-natural">
							<h2>Natural</h2>
						</div>
						<div class="box-content">
							<div class="row">
                                <?php
                                    $natural_posts = new WP_Query(array(
                                       'posts_type'     => 'post',
                                       'posts_per_page' =>2,
                                       'category_name' =>'natural'
                                    ));
                                    if ($natural_posts->have_posts()):
                                        while ($natural_posts->have_posts()):$natural_posts->the_post()
                                ?>
								<div class="col-md-6">
									<?php the_post_thumbnail()?>
									<h3><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>
									<span><i class="fa fa-heart"></i> 1,200 / <i class="fa fa-calendar"></i> <?php the_date('F j, Y');?> / <i class="fa fa-comment-o"> / </i> 10 <i class="fa fa-eye"></i> 945</span>
									<span class="rating">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half"></i>
									</span>
									<p><?php the_excerpt()?></p>
								</div>
                                <?php endwhile;endif;wp_reset_query();?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php get_sidebar('home-page')?>
		</div>
	</div>

<?php get_footer()?>
