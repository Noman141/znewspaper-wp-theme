<footer>
    <div class="wrap-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-footer footer-1">
                    <?php dynamic_sidebar('footer_left')?>
                </div>
                <div class="col-md-4 col-footer footer-2">
                    <?php dynamic_sidebar('footer_middle')?>
                </div>
                <div class="col-md-4 col-footer footer-3">
                    <?php dynamic_sidebar('footer_right')?>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <?php dynamic_sidebar('copy_right')?>
    </div>
</footer>
<!-- Footer -->

<!-- JS -->
<script src="<?php echo get_template_directory_uri()?>/owl-carousel/owl.carousel.js"></script>



<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/custom.js"></script>
<?php wp_footer()?>
</body>
</html>